<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

$shortcode_params = array(
    array(
        'type' => 'attach_image',
        'heading' => __('Upload the banner image', 'veera'),
        'param_name' => 'banner_id'
    ),
    array(
        'type' => 'dropdown',
        'heading' => __('Height Mode','veera'),
        'param_name' => 'height_mode',
        'value' => array(
            __('1:1','veera') => '1-1',
            __('Original','veera') => 'original',
            __('4:3','veera') => '4-3',
            __('3:4','veera') => '3-4',
            __('16:9','veera') => '16-9',
            __('9:16','veera') => '9-16',
            __('Custom','veera') => 'custom',
        ),
        'std' => 'original',
        'description' => __('Sizing proportions for height and width. Select "Original" to scale image without cropping.', 'veera')
    ),
    array(
        'type' 			=> 'textfield',
        'heading' 		=> __('Height', 'veera'),
        'param_name' 	=> 'height',
        'value'			=> '',
        'description' 	=> __('Enter custom height.', 'veera'),
        'dependency' => array(
            'element'   => 'height_mode',
            'value'     => array('custom')
        )
    ),
    array(
        'type' => 'dropdown',
        'heading' => __('Design','veera'),
        'param_name' => 'style',
        'value' => array(
            __('Style 1','veera') => '1',
            __('Style 2','veera') => '2',
            __('Style 3','veera') => '3',
            __('Style 4','veera') => '4',
            __('Style 5','veera') => '5',
            __('Style 6','veera') => '6',
            __('Style 7','veera') => '7'
        ),
        'std' => '1'
    ),

    array(
        'type' => 'vc_link',
        'heading' => __('Banner Link', 'veera'),
        'param_name' => 'banner_link',
        'description' => __('Add link / select existing page to link to this banner', 'veera')
    ),


    array(
        'type' => 'textfield',
        'heading' => __( 'Banner Title 1', 'veera' ),
        'param_name' => 'title_1',
        'admin_label' => true
    ),

    array(
        'type' => 'textfield',
        'heading' => __( 'Banner Title 2', 'veera' ),
        'param_name' => 'title_2',
        'admin_label' => true,
        'dependency' => array(
            'element' => 'style',
            'value' => array('6','7')
        ),
    ),
    array(
        'type' => 'textfield',
        'heading' => __( 'Banner Title 3', 'veera' ),
        'param_name' => 'title_3',
        'admin_label' => true,
        'dependency' => array(
            'element' => 'style',
            'value' => array('7')
        ),
    ),

    LaStudio_Shortcodes_Helper::fieldElementID(array(
        'param_name' 	=> 'el_id'
    )),

    LaStudio_Shortcodes_Helper::fieldExtraClass(),
    LaStudio_Shortcodes_Helper::fieldExtraClass(array(
        'heading' 		=> __('Extra class name for title 1', 'veera'),
        'param_name' 	=> 'el_class1',
    )),
    LaStudio_Shortcodes_Helper::fieldExtraClass(array(
        'heading' 		=> __('Extra class name for title 2', 'veera'),
        'param_name' 	=> 'el_class2',
        'dependency' => array(
            'element' => 'style',
            'value' => array('6','7')
        )
    )),
    LaStudio_Shortcodes_Helper::fieldExtraClass(array(
        'heading' 		=> __('Extra class name for title 3', 'veera'),
        'param_name' 	=> 'el_class3',
        'dependency' => array(
            'element' => 'style',
            'value' => array('7')
        )
    )),
    array(
        'type' 			=> 'colorpicker',
        'param_name' 	=> 'overlay_bg_color',
        'heading' 		=> __('Overlay background color', 'veera'),
        'group' 		=> 'Design'
    ),
    array(
        'type' 			=> 'colorpicker',
        'param_name' 	=> 'overlay_hover_bg_color',
        'heading' 		=> __('Overlay hover background color', 'veera'),
        'group' 		=> 'Design'
    ),
    array(
        'type' 			=> 'colorpicker',
        'param_name' 	=> 'btn_color',
        'heading' 		=> __('Button Color', 'veera'),
        'group' 		=> 'Design'
    ),
    array(
        'type' 			=> 'colorpicker',
        'param_name' 	=> 'btn_bg_color',
        'heading' 		=> __('Button Background Color', 'veera'),
        'group' 		=> 'Design'
    ),
    array(
        'type' 			=> 'colorpicker',
        'param_name' 	=> 'btn_hover_color',
        'heading' 		=> __('Button Hover Color', 'veera'),
        'group' 		=> 'Design'
    ),
    array(
        'type' 			=> 'colorpicker',
        'param_name' 	=> 'btn_hover_bg_color',
        'heading' 		=> __('Button Hover Background Color', 'veera'),
        'group' 		=> 'Design'
    ),
);

$param_fonts_title1 = LaStudio_Shortcodes_Helper::fieldTitleGFont('title_1', __('Title 1', 'veera'));
$param_fonts_title2 = LaStudio_Shortcodes_Helper::fieldTitleGFont('title_2', __('Title 2', 'veera'), array(
    'element' => 'style',
    'value' => array('6','7')
));
$param_fonts_title3 = LaStudio_Shortcodes_Helper::fieldTitleGFont('title_3', __('Title 3', 'veera'), array(
    'element' => 'style',
    'value' => array('7')
));


$shortcode_params = array_merge( $shortcode_params, $param_fonts_title1, $param_fonts_title2, $param_fonts_title3);

return apply_filters(
    'LaStudio/shortcodes/configs',
    array(
        'name'			=> __('Banner Box', 'veera'),
        'base'			=> 'la_banner',
        'icon'          => 'la-wpb-icon la_banner',
        'category'  	=> __('La Studio', 'veera'),
        'description'   => __('Displays the banner image with Information', 'veera'),
        'params' 		=> $shortcode_params
    ),
    'la_banner'
);