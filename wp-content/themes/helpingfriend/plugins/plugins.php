<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_action( 'tgmpa_register', 'veera_register_required_plugins' );

if(!function_exists('veera_register_required_plugins')){

	function veera_register_required_plugins() {

		$plugins = array();

		$plugins[] = array(
			'name'					=> esc_html_x('WPBakery Visual Composer', 'admin-view', 'veera'),
			'slug'					=> 'js_composer',
			'source'				=> get_template_directory() . '/plugins/js_composer.zip',
			'required'				=> true,
			'version'				=> '5.7'
		);

		$plugins[] = array(
			'name'					=> esc_html_x('LA-Studio Core', 'admin-view', 'veera'),
			'slug'					=> 'lastudio',
			'source'				=> get_template_directory() . '/plugins/lastudio.zip',
			'required'				=> true,
			'version'				=> '1.0.7'
		);

		$plugins[] = array(
			'name'     				=> esc_html_x('WooCommerce', 'admin-view', 'veera'),
			'slug'     				=> 'woocommerce',
			'version'				=> '3.6.1',
			'required' 				=> false
		);

		$plugins[] = array(
			'name'     				=> esc_html_x('Envato Market', 'admin-view', 'veera'),
			'slug'     				=> 'envato-market',
			'source'   				=> 'https://envato.github.io/wp-envato-market/dist/envato-market.zip',
			'required' 				=> false,
			'version' 				=> '2.0.1'
		);

		$plugins[] = array(
			'name'					=> esc_html_x('Veera Package Demo Data', 'admin-view', 'veera'),
			'slug'					=> 'veera-demo-data',
			'source'				=> 'https://github.com/la-studioweb/resource/raw/master/veera/veera-demo-data.zip',
			'required'				=> true,
			'version'				=> '1.0.1'
		);

		$plugins[] = array(
			'name' 					=> esc_html_x('Contact Form 7', 'admin-view', 'veera'),
			'slug' 					=> 'contact-form-7',
			'required' 				=> false
		);

		$plugins[] = array(
			'name'					=> esc_html_x('Slider Revolution', 'admin-view', 'veera'),
			'slug'					=> 'revslider',
			'source'				=> get_template_directory() . '/plugins/revslider.zip',
			'required'				=> false,
			'version'				=> '5.4.8.3'
		);

		$config = array(
			'id'           				=> 'veera',
			'default_path' 				=> '',
			'menu'         				=> 'tgmpa-install-plugins',
			'has_notices'  				=> true,
			'dismissable'  				=> true,
			'dismiss_msg'  				=> '',
			'is_automatic' 				=> false,
			'message'      				=> ''
		);

		tgmpa( $plugins, $config );

	}

}
