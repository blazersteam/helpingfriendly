<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $veera_loop;

$tmp = $veera_loop;
$veera_loop = array();

$loop_layout = Veera()->settings()->get('portfolio_display_type', 'grid');
$loop_style = Veera()->settings()->get('portfolio_display_style', '1');

$height_mode        = veera_get_theme_loop_prop('height_mode', 'original');
$thumb_custom_height= veera_get_theme_loop_prop('height', '');

veera_set_theme_loop_prop('is_main_loop', true, true);
veera_set_theme_loop_prop('loop_layout', $loop_layout);
veera_set_theme_loop_prop('loop_style', $loop_style);
veera_set_theme_loop_prop('responsive_column', Veera()->settings()->get('portfolio_column', array('xlg'=> 1, 'lg'=> 1,'md'=> 1,'sm'=> 1,'xs'=> 1)));
veera_set_theme_loop_prop('image_size', Veera_Helper::get_image_size_from_string(Veera()->settings()->get('portfolio_thumbnail_size', 'full'),'full'));
veera_set_theme_loop_prop('title_tag', 'h4');
veera_set_theme_loop_prop('excerpt_length', '15');
veera_set_theme_loop_prop('item_space', Veera()->settings()->get('portfolio_item_space', 'default'));
veera_set_theme_loop_prop('height_mode', Veera()->settings()->get('portfolio_thumbnail_height_mode', 'original'));
veera_set_theme_loop_prop('height', Veera()->settings()->get('portfolio_thumbnail_height_custom', ''));

echo '<div id="archive_portfolio_listing" class="la-portfolio-listing">';

if( have_posts() ){

    get_template_part("templates/portfolios/start", $loop_style);

    while( have_posts() ){

        the_post();

        get_template_part("templates/portfolios/loop", $loop_style);

    }

    get_template_part("templates/portfolios/end", $loop_style);

}

echo '</div>';
/**
 * Display pagination and reset loop
 */

veera_the_pagination();

wp_reset_postdata();

$veera_loop = $tmp;