<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


if (!function_exists('la_log')) {
    function la_log($log) {
        if (true === WP_DEBUG) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log($log);
            }
        }
    }
}

/**
 * Require plugins vendor
 */

require_once get_template_directory() . '/plugins/tgm-plugin-activation/class-tgm-plugin-activation.php';
require_once get_template_directory() . '/plugins/plugins.php';

/**
 * Include the main class.
 */

include_once get_template_directory() . '/framework/classes/class-core.php';


Veera::$template_dir_path   = get_template_directory();
Veera::$template_dir_url    = get_template_directory_uri();
Veera::$stylesheet_dir_path = get_stylesheet_directory();
Veera::$stylesheet_dir_url  = get_stylesheet_directory_uri();

/**
 * Include the autoloader.
 */
include_once Veera::$template_dir_path . '/framework/classes/class-autoload.php';

new Veera_Autoload();

/**
 * load functions for later usage
 */

require_once Veera::$template_dir_path . '/framework/functions/functions.php';

new Veera_Multilingual();

if(!function_exists('Veera')){
    function Veera(){
        return Veera::get_instance();
    }
}

new Veera_Scripts();

new Veera_Admin();

new Veera_WooCommerce();

new Veera_WooCommerce_Wishlist();

new Veera_WooCommerce_Compare();

new Veera_Visual_Composer();

/**
 * Set the $content_width global.
 */
global $content_width;
if ( ! is_admin() ) {
    if ( ! isset( $content_width ) || empty( $content_width ) ) {
        $content_width = (int) Veera()->layout()->get_content_width();
    }
}

require_once Veera::$template_dir_path . '/framework/functions/extra-functions.php';

require_once Veera::$template_dir_path . '/framework/functions/update.php';
