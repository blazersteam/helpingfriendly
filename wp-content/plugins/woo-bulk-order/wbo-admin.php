<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function wpdocs_theme_name_scripts() {

	// Make sure jquery is added
	if ( ! wp_script_is( 'jquery', 'done' ) ) {
     	wp_enqueue_script( 'jquery' );
   	}

    wp_enqueue_style( 'wbo-style', plugin_dir_url( __FILE__ ) . 'assets/css/front.css'  );
    wp_enqueue_style( 'wbo-select2', plugin_dir_url( __FILE__ ) . 'assets/css/select2.css'  );
    wp_enqueue_script( 'wbo-js', plugin_dir_url( __FILE__ ) . 'assets/js/front.js', array('jquery'), '1.0.0', true );

    wp_enqueue_script( 'wbo-select2', plugin_dir_url( __FILE__ ) . 'assets/js/select2.js', array('jquery'), '1.0.0', true );

    $translation_array = array(
		'ajax_url' => admin_url('admin-ajax.php')
	);
	wp_localize_script( 'wbo-js', 'obj', $translation_array );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

add_shortcode('wbo_woo_bulk_order', 'wbo_woo_bulk_order_shortcode');
function wbo_woo_bulk_order_shortcode( $atts = []) {

	global $wpdb;

	$atts = array_change_key_case((array)$atts, CASE_LOWER);

	if(isset($_REQUEST['status']))
		return;

	$strProd = "SELECT p.ID, p.post_title, pm.meta_value attributes
				FROM {$wpdb->prefix}posts p
				LEFT JOIN {$wpdb->prefix}postmeta pm ON (pm.post_id = p.ID AND pm.meta_key = '_product_attributes')
				WHERE p.post_type = 'product' 
				AND p.post_status = 'publish'" . 
				(isset($atts['id']) && !empty($atts['id'])?" AND p.ID IN (" . $atts['id'] . ")":'');
	$arrProd = $wpdb->get_results($strProd);

	$arrProduct = array();
	foreach ($arrProd as $key => $value) {
		
		$fold_num = rand(-1000,-100);
		if(isset($value->attributes) && !empty($value->attributes)) {
			$arrAttr = unserialize($value->attributes);
			if(isset($arrAttr['foldernummer']) && !empty($arrAttr['foldernummer'])) {
				$fold_num = $arrAttr['foldernummer']['value'];
			}
		}
		$arrProduct[$fold_num]['name'] = $value->post_title;
		$arrProduct[$fold_num]['ID'] = $value->ID;
	}
	krsort($arrProduct, SORT_NUMERIC);

	$result = '<div class="wbo_wrapper">
				<div class="wbo_row">
					<div class="wbo_thumb">&nbsp;</div>
					<div class="wbo_name"><strong>Product name</strong></div>
					<div class="wbo_price"><strong>Price</strong></div>
					<div class="wbo_quantity"><strong>Quantity</strong></div>
					<div class="wbo_add_to_cart"></div>
					<div class="wbo_loader"></div>
					<div class="wbo_completed"></div>
					<div class="clear"></div>
				</div>';

	foreach ($arrProduct as $key => $value) {
		
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
		if(!isset($image[0]) || empty($image)) {
			$image[0] = wc_placeholder_img_src('single-post-thumbnail');
		}
		$product = wc_get_product($value['ID']);
//print_r($product);
		$loop = 0;
		$result1 .= '<div class="wbo_row">
			<div class="wbo_thumb">
				<img src="'. $image[0] . '" title="' . $value->post_title . '" style="height:100px;width:100px;"/>
			</div>
			<div class="wbo_name">';

			if(is_a($product, 'WC_Product_Variable')) {
				$result1 .= '<select id=\'sel_bulk_order[' . $value['ID'] . ']\' name=\'sel_bulk_order[' . $value['ID'] . ']\' style="width:100%" class="wbo-options"">
				<option value="" disabled="disabled">Choose Variation</option>';


				$objProduct = wc_get_product($value['ID']);
				$type = $objProduct->get_type();

				if($type == 'variable') {
					
					// Get variations
					$strVariation = "SELECT post_title, ID FROM {$wpdb->prefix}posts WHERE post_type = 'product_variation' AND post_parent = '" . $value['ID'] . "'";
					$arrVariation = $wpdb->get_results($strVariation);
					foreach ($arrVariation as $keyVar => $valueVar) {
						$valueOption = $valueVar->ID;

						$objVariation = new WC_Product_Variation($valueVar->ID);

						// make sure we pass correct price html
						$price_html = str_replace("'", '"', $objVariation->get_price_html());
						$result1 .=  '<option attr-pid="'. $value['ID'] .'" value="' . $valueOption  . '" attr-price=\'' . $price_html . '\'>' . $valueVar->post_title . '</option>';
					}
				}
			$result1 .= '</select>';
				//$result1 .= $value['name'];
			} else {
				$result1 .= $value['name'];
			}
			$result1 .= '</div>
			<div class="wbo_price" style="display:none;">' . wc_price($product->get_price()) . '</div>
			<div class="wbo_quantity wbo_qty_' . $value['ID'] . '">
				<input type="number" name="qty_' . $value['ID'] . '" id="qty_' . $value['ID'] . '" min="1" value="1"/>
			</div>
			<div class="wbo_var_id wbo_varid_' . $value['ID'] . '" style="display:none;">
				<input type="number" name="varid_' . $value['ID'] . '" id="varid_' . $value['ID'] . '" min="1" value="1"/>
			</div>
			
			<div class="wbo_add_to_cart"><input type="button" name="add-to-cart" value="' . esc_html( $product->single_add_to_cart_text()) . '" class="single_add_to_cart_button button alt wbo_button" data-p_id="' . $value['ID'] . '" id="btn_' . $value['ID'] . '"/>
			</div>
			<div class="wbo_loader loader_' . $value['ID'] . '" style="display:none">
				<div class="row">
					<div class="col-sm-2 col-xs-4 text-center">
						<div class="three-quarters-loader"></div>
					</div>
				</div>
			</div>
			<div class="wbo_completed complete_' . $value['ID'] . '" style="display:none">
				<img src="' . plugin_dir_url( __FILE__ ) . 'assets/images/complete.png' . '" style="height:25px;width:25px;">
			</div>
			<div class="clear"></div>
		</div>';
	}
	$result1 .= '</div><div class="view-cart"><a class="view_cart_btn" href="/cart/">View Cart</a></div>';
	return $result1;
}

add_action( 'wp_ajax_wbo_add_to_cart', 'wbo_add_to_cart' );
add_action( 'wp_ajax_nopriv_wbo_add_to_cart', 'wbo_add_to_cart' );
function wbo_add_to_cart() {

	$quantity = $_REQUEST['qty'];
	$product_id = $_REQUEST['p_id'];
		$variationId = $_REQUEST['varid'];
	if($_REQUEST['varid']==1){
		$variationId = 0;
	}
	
	WC()->cart->add_to_cart( $product_id, $quantity,$variationId);
	die();
}
