jQuery('.wbo_button').on( 'click', function($) {
	
	var var_id = '';
	if(jQuery(this).parent().prev().prev().prev().find('select') != undefined) {
		var_id = jQuery(this).parent().prev().prev().prev().find('select').val();
	}

	jQuery(this).parent().next().fadeIn(200);
	var p_id = jQuery(this).attr('data-p_id');

	if (typeof var_id !== 'undefined'&&var_id !== ''&&var_id == null){
		p_id=var_id;
	}
	
	// get quantity
	var qty = jQuery(document).find('.wbo_qty_' + p_id).find('input').val();
	// get variation Id
	var varid = jQuery(document).find('.wbo_varid_' + p_id).find('input').val();
	console.log(varid);
//'p_id': var_id != ''?var_id:p_id
	var data = {
		'action': 'wbo_add_to_cart',
		'qty': qty,
		'p_id': p_id,
		'varid': varid
	};
	// We can also pass the url value separately from ajaxurl for front end AJAX implementations
	jQuery.post(obj.ajax_url, data, function(response) {
		jQuery(document).find('.complete_' + p_id).fadeIn(200);
		jQuery(document).find('.loader_' + p_id).fadeOut(50);

		refresh_fragments();
		setTimeout(function() {
        	jQuery(document).find('.complete_' + p_id).fadeOut(300);
        }, 3000);
	});
});

function refresh_fragments() {
    jQuery( document.body ).trigger( 'wc_fragment_refresh' );
}
if(jQuery != undefined) {
	jQuery(document).ready(function() {
		jQuery('.wbo-options').each(function() {
			jQuery(this).select2();
			wbo_check_variation_price(jQuery(this));	
		});
	});
} else {
	alert('You may face difficulties using this page as your theme does not use jQuery.')
}

jQuery('.wbo-options').on('change', function() {
	wbo_check_variation_price(jQuery(this));
});

// Update prices for selected variation
function wbo_check_variation_price(objSelect) {
	var option = jQuery('option:selected', objSelect).attr('attr-price');
	jQuery(document).find('.wbo_varid_'+jQuery('option:selected', objSelect).attr('attr-pid')).find('input').val(jQuery('option:selected', objSelect).attr('value'));
	objSelect.parent().next().html(option);
}