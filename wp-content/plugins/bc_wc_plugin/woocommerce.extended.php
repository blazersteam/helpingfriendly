<?php 
include_once BLOCKCHAIN_PLUGIN_DIR.'skymids.api.php';
include_once BLOCKCHAIN_PLUGIN_DIR.'blockchain.api.php';
use BCWC_PLUGIN as Quickcard;
	/**
	 * Invoice Payment Gateway
	 *
	 */
	class WC_Gateway_Block_Chain extends WC_Payment_Gateway {
	    /**
	     * Constructor for the gateway.
	     *
	     */
		private $__skymidsObj;
		private $__blockChainObj;
		public function __construct() {
	        $this->id		= 'blockchainpayment';
	        $this->icon 		= apply_filters('woocommerce_invoice_icon', '');
	        $this->has_fields 	= true;
	        $this->method_title     = __( 'Block Chain Gateway', 'blockChain' );
			$this->__skymidsObj=Quickcard\SkymidsApi::getInstance();
			$this->__blockChainObj=BlockChainApi::getInstance();

			$this->supports = array( 
				'products', 
				'subscriptions',
				'subscription_cancellation', 
				//'subscription_suspension', 
				//'subscription_reactivation',
				//'subscription_amount_changes',
				//'subscription_date_changes',
				//'subscription_payment_method_change'
		   );
			// Load the form fields.
			$this->init_form_fields();
	
			// Load the settings.
			$this->init_settings();
	
			// Define user set variables
			$this->title = $this->settings['title'];
			$this->description = $this->settings['description'];
			$this->emailmsg = $this->settings['emailmsg'];
			$this->blockchainsandbox=$this->settings['blockchainsandbox'];
			$this->blockchianclientId=$this->settings['quickcard_client_id'];
			$this->blockchiansecretkey=$this->settings['quickcard_client_secret'];
			$this->blockchianlocationid=$this->settings['quickcard_client_location'];
			// $this->skymidsaccesstoken=$this->settings['skymidsaccesstoken'];

	
			// Actions
			//add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));
	    	add_action('woocommerce_thankyou_directdebit', array(&$this, 'thankyou_page'));
	    	/** Detecting WC version **/
			if ( version_compare( WOOCOMMERCE_VERSION, '2.0', '<' ) ) {
			  add_action( 'woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );
			} else {
			  add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options'),10,1);
			}
	    	// Customer Emails
			add_action('woocommerce_email_before_order_table', array(&$this, 'email_instructions'), 10, 2);
			add_action('wp_enqueue_scripts', array($this,'load_quickcard_scripts'));
			add_action( 'woocommerce_receipt_' . $this->id, array($this, 'receipt_page'));
			add_action( 'woocommerce_api_wc_gateway_block_chain', array( $this, 'bc_verify_payment' ) );
			add_action( 'woocommerce_api_wc_gateway_success_page', array($this, 'success_gateway_response'));
			add_action( 'woocommerce_api_wc_gateway_failure_page', array($this, 'failure_gateway_response'));
	    }
	    /**
	     * Initialise Gateway Settings Form Fields
	     *
	     */
	    function init_form_fields() {
	    	$this->form_fields = array(
				'enabled' => array(
					'title' => __( 'Enable/Disable', 'blockChain' ),
					'type' => 'checkbox',
					'label' => __( 'Enable Block Chain Payment', 'blockChain' ),
					'default' => 'yes'
							),
				'title' => array(
					'title' => __( 'Title', 'blockChain' ),
					'type' => 'text',
					'description' => __( 'This controls the title which the user sees during checkout.', 'blockChain' ),
					'default' => __( 'Block Chain Payment', 'blockChain' )
							),
				'description' => array(
					'title' => __( 'Customer Message', 'blockChain' ),
					'type' => 'textarea',
					'description' => __( 'Enter the description that goes on order-review page after the order has been submited' ),
					'default' => __( 'Add some description here', 'blockChain' )
							),
				'emailmsg' => array(
					'title' => __( 'Order Email Message', 'blockChain' ),
					'type' => 'textarea',
					'description' => __( 'Enter the message that is sent on the order-confirmation email after the order has been submited' ),
					'default' => __( 'Add some description here', 'blockChain' )
				),
				'blockchainsandbox' => array(
					'title' => __( 'Block Chain sandbox ', 'blockChain' ),
					'type' => 'checkbox',
					'description' => __( 'Block Chain Sandbox can be used to test payments.' ),
					'label' => __( 'Enable Block Chain sandbox', 'blockChain' ),
					'default' => 'no'
							),
							
				'blockchianclientId' => array(
					'title' => __( 'Client ID', 'blockChain' ),
					'type' => 'text',
					'default' => __('','blockChain')
				),
				'blockchiansecretkey' => array(
					'title' => __( 'Secret Key', 'blockChain' ),
					'type' => 'text',
					'default' => __('','blockChain')
				),	
				'blockchianlocationid' => array(
					'title' => __( 'Location ID', 'blockChain' ),
					'type' => 'text',
					'default' => __('','blockChain')
				)	
				);
	
	    }
		/**
		 * Admin Panel Options
		 * - Options for bits like 'title' and availability on a country-by-country basis
		 *
		 */
		public function admin_options() {
	
	    	?>
	    	<h3><?php _e('Block Chain Gateway', 'blockChain'); ?></h3>
	    	<p><?php _e('Allows customer to enter the IBAN and BIC number and the proceed with payment. This method is also called Direct Debits.', 'blockChain'); ?></p>
	    	<table class="form-table">
	    	<?php
	    		// Generate the HTML For the settings form.
	    		$this->generate_settings_html();
	    	?>
			</table><!--/.form-table-->
	    	<?php
	    }
	    /**
	     * Output for the order received page.
	     *
	     */
		function thankyou_page() {
			if ( $emailmsg = $this->emailmsg )
	        	echo wpautop( wptexturize( $emailmsg ) );
		}
	
	    /**
	     * Add content to the WC emails.
	     *
	     */
		function email_instructions( $order, $sent_to_admin ) {
	    	if ( $sent_to_admin ) return;
	
	    	if ( $order->status !== 'on-hold') return;
	
	    	if ( $order->payment_method !== 'blockchainpayment') return;
	
			if ( $emailmsg = $this->emailmsg )
	        	echo wpautop( wptexturize( $emailmsg ) );
		}
	    /**
	     * Process the payment and return the result
	     *
	     */
		function process_payment( $order_id ) {
		// ----------------------------get payment settings---------------------- 
		 $order = wc_get_order( $order_id );
		 $payment_gateway="";
		 $payment_gateway_second="";
		 $oid="";
         $blockChainInstance=get_option('woocommerce_blockchainpayment_settings');
         $location_id=$blockChainInstance['blockchianlocationid'];
         $this->__blockChainObj->selectApiUrl($blockChainInstance['blockchainsandbox']);
			$access_token=$this->__blockChainObj->getAccessToken($blockChainInstance['blockchianclientId'],$blockChainInstance['blockchiansecretkey'],$blockChainInstance['blockchianlocationid']);
		if($access_token=="Unauthorized" || $access_token['payment_gateway']==""){
			return array(
							'result'   => 'failed',
							'redirect' => $order->get_cancel_order_url_raw()
							);	
		}

		if($access_token!="Unauthorized"){	
			$auth_token 				=   $access_token['access_token'];
			$payment_gateway			=	$access_token['payment_gateway'];
			$payment_gateway_second		=	$access_token['second_payment_gateway'];
		}
		if($payment_gateway=="i_can_pay"){
			// --------------------------3ds Payment Added--------------------------
			$billing_state = $order->get_billing_state();
	        $billing_postcode = $order->get_billing_postcode();
            
	        $billing_state = !empty($billing_state) ? $billing_state : 'xx';
	        $billing_postcode = !empty($billing_postcode) ? $billing_postcode : '1234';

	        $totalAmount = $order->get_total();

	        $gateway_params = array(
	        	"auth_token"        => $auth_token,
	        	"location_id"       => $location_id,
	            "order_id"          => $order_id,
	            "transaction_type"  => 'a',
	            "amount"            => $totalAmount,
	            "currency"          => get_option('woocommerce_currency'),
	            "first_name"        => $order->get_billing_first_name(),
	            "last_name"         => $order->get_billing_last_name(),
	            "email"             => $order->get_billing_email(),
	            "street"            => $order->get_billing_address_1(),
	            "city"              => $order->get_billing_city(),
	            "zip_code"          => $billing_postcode,
	            "state"             => strlen($billing_state) > 2 ? substr($billing_state, 0, 2) : $billing_state,
	            "country"           => $this->get_country_iso3($order->get_billing_country()),
	            "phone_number"      => preg_replace('/[^0-9]/', '', $order->get_billing_phone()),
	            "dob"               => date('Y-m-d', strtotime('- 25 years')),
	            "name"              => $order->get_billing_first_name().' '.$order->get_billing_last_name(),
	            "success_url"       => urlencode(esc_url_raw(WC()->api_request_url('wc_gateway_success_page'))),
	            "fail_url"          => urlencode(esc_url_raw(WC()->api_request_url('wc_gateway_failure_page'))),
	            "notify_url"        => urlencode(esc_url_raw(WC()->api_request_url('wc_gateway_failure_page'))),
	        );

		        $endpoint="/api/registrations/virtual_transaction_3ds";
		        $response=$this->__blockChainObj->curl($gateway_params,$endpoint);
		        if($response["success"]==true){
		        	if($response["redirect_url"]==""){
			            $order->payment_complete($order_id);
						$order->set_transaction_id($response['transaction_id']);
						update_post_meta($order_id, '_blockchain_transaction_id', $response['transaction_id']);
						$order->add_order_note( "Payment processed and approved successfully with reference:". $response['transaction_id']);
						WC()->cart->empty_cart();
						return array(
				  			'result'   => 'success',
				  			'redirect' => $this->get_return_url( $order )
						);
		        	}else{
		        		    if($response["gateway"]){
		        		    	if ( get_option('GATEWAYSECURE') !== false ) {
								    update_option('GATEWAYSECURE', $response["gateway"]);
								} else {
								    $deprecated = null;
								    $autoload = 'no';
								    add_option('GATEWAYSECURE', $response["gateway"], $deprecated, $autoload );
								}
		        		    }
		        			return array(
										  'result'   => 'success',
										  'redirect' => urldecode($response['redirect_url'])
							);		        		
		        	}
		        }else{
		        	if($response["transaction_id"]){
		        		$oid=$order_id.'-'.$response["transaction_id"];
		        	}
		        	$i_can_pay_secure=$order_id.'prev';
		        	if (get_option('SECONDGATEWAY') !== false) {
							update_option('SECONDGATEWAY', $i_can_pay_secure);
					} else {
					    	$deprecated = null;
					    	$autoload = 'no';
					    	add_option('SECONDGATEWAY', $i_can_pay_secure, $deprecated, $autoload );
					}
		            //       return array(
					// 					  'result'   => 'failed',
					// 					  'redirect' => $order->get_cancel_order_url_raw()
					//		 );
					if($payment_gateway_second!=""){
						return array(
									'result'   => 'success',
									'redirect' =>  $order->get_checkout_payment_url(true)
					    );
					}else{
						return $this->failure_gateway_response($oid,$order->get_cancel_order_url_raw());
					}
		        }
		        // ------------------------3ds payment Failed--------------------------------
		    }else{
				return array(
			  		'result'   => 'success',
			  		'redirect' => $order->get_checkout_payment_url( true )
			);
		 }
		}
		//-------------------------------------Success From Gateway-------------------------- 
		function success_gateway_response(){
	         $oid=$_REQUEST['oid'];
	         $explode_order_id=explode("-", $oid);
	         $order_id=$explode_order_id[0];
	         $transaction_id=$explode_order_id[1];
	         $blockChainInstance=get_option('woocommerce_blockchainpayment_settings');
	         $location_id=$blockChainInstance['blockchianlocationid'];
	         $this->__blockChainObj->selectApiUrl($blockChainInstance['blockchainsandbox']);
				$access_token=$this->__blockChainObj->getAccessToken($blockChainInstance['blockchianclientId'],$blockChainInstance['blockchiansecretkey'],$blockChainInstance['blockchianlocationid']);
			 if($access_token!="Unauthorized"){	
				 $auth_token = $access_token['access_token'];
				 $payment_gateway=	$access_token['payment_gateway'];
			 }else{
			 	wp_redirect($order->get_cancel_order_url_raw());
			 }
		    	if ( get_option('GATEWAYSECURE') !== false ) {
				    $gateway=get_option('GATEWAYSECURE');
				} else {
				    $gateway="";
				}		
			 $order = wc_get_order($order_id);
			 $totalAmount = $order->get_total();
			   $gateway_params = array(
		        	"auth_token"        => $auth_token,
		        	"location_id"       => $location_id,
		        	"transaction_id"    => $transaction_id,
		        	"phone_number"      => $order->get_billing_phone(),
		        	"gateway"           => $gateway,
		        	"amount"            => $totalAmount
		      );
	         $endpoint="/api/registrations/virtual_transaction_3ds_success";
	         $response=$this->__blockChainObj->curl($gateway_params,$endpoint);
	         if($response["success"]==true){
		         $order->payment_complete( $order_id );
				 $order->set_transaction_id($transaction_id);
				 update_post_meta( $order_id, '_blockchain_transaction_id', $transaction_id);
				 $order->add_order_note( "Payment processed and approved successfully with reference:". $transaction_id);
				 WC()->cart->empty_cart();
		         wp_redirect($this->get_return_url($order));
	         }else{
	         	 wp_redirect($order->get_cancel_order_url_raw());
	         }
		}
		//-------------------------------------Failure From Gateway-------------------------- 
		function failure_gateway_response($order_id=null,$order_url=null){
			 if($order_id!="" && $order_id!=null){
			 	$oid=$order_id;
			 }else{
	         	$oid=$_REQUEST['oid'];
	         }
	         $explode_order_id=explode("-", $oid);
	         $order_id=$explode_order_id[0];   //order_id    
	         $transaction_id=$explode_order_id[1]; //db_transaction_id
	         $blockChainInstance=get_option('woocommerce_blockchainpayment_settings');
	         $location_id=$blockChainInstance['blockchianlocationid'];
	         $this->__blockChainObj->selectApiUrl($blockChainInstance['blockchainsandbox']);
				$access_token=$this->__blockChainObj->getAccessToken($blockChainInstance['blockchianclientId'],$blockChainInstance['blockchiansecretkey'],$blockChainInstance['blockchianlocationid']);
			 if($access_token!="Unauthorized"){	
				 $auth_token = $access_token['access_token'];
				 $payment_gateway=	$access_token['payment_gateway'];
			 }else{
			 	if($order_url==null){
			 		wp_redirect($order->get_cancel_order_url_raw());
			 	}else{
			 		 return array(
										  'result'   => 'success',
										  'redirect' => $order_url
							 );
			 	}
			 }
			 $order = wc_get_order($order_id);
			   $gateway_params = array(
		        	"auth_token"        => $auth_token,
		        	"location_id"       => $location_id,
		        	"transaction_id"    => $transaction_id,
		        	"phone_number"      => $order->get_billing_phone()
		      );
	         $endpoint="/api/registrations/virtual_transaction_3ds_failure";
	         $response=$this->__blockChainObj->curl($gateway_params,$endpoint);
	         if($order_url==null){
			 		wp_redirect($order->get_cancel_order_url_raw());
			 	}else{
			 		 return array(
										  'result'   => 'success',
										  'redirect' => $order_url
							 );
			 	}
		}

		function process_admin_options(){
			$this->init_settings();
			$emptyData=false;
			if(!isset($_POST['woocommerce_blockchainpayment_blockchianclientId']) || empty($_POST['woocommerce_blockchainpayment_blockchianclientId']) || !isset($_POST['woocommerce_blockchainpayment_blockchiansecretkey']) || empty($_POST['woocommerce_blockchainpayment_blockchiansecretkey'])){
				$_POST['woocommerce_blockchainpayment_blockchianclientId']='';
				$_POST['woocommerce_blockchainpayment_blockchiansecretkey']='';
				$_POST['woocommerce_blockchainpayment_blockchianlocationid']='';
				$emptyData=true;
			}
			$url="no";
			if(isset($_POST['woocommerce_blockchainpayment_blockchainsandbox'])){
				$url="yes";
			}
		    if($emptyData==false){
				$client_id=$_POST['woocommerce_blockchainpayment_blockchianclientId'];
				$client_secret=$_POST['woocommerce_blockchainpayment_blockchiansecretkey'];
				$location_id=$_POST['woocommerce_blockchainpayment_blockchianlocationid'];
				$this->__blockChainObj->selectApiUrl($url);
				$token_api_response=$this->__blockChainObj->getAccessToken($client_id,$client_secret,$location_id);
				if($token_api_response=='Unauthorized'){
					$_POST['woocommerce_blockchainpayment_blockchianclientId']='';
					$_POST['woocommerce_blockchainpayment_blockchiansecretkey']='';
					$_POST['woocommerce_blockchainpayment_blockchianlocationid']='';
				}
			}
			// $qc_api=new BlockChainApi();
            $post_data = $this->get_post_data();
            foreach ( $this->get_form_fields() as $key => $field ) {
              if ( 'title' !== $this->get_field_type( $field ) ) {
                try {
                  $this->settings[ $key ] = $this->get_field_value( $key, $field, $post_data );
                } catch ( Exception $e ) {
                  $this->add_error( $e->getMessage() );
                }
              }
            }
            return update_option( $this->get_option_key(), apply_filters( 'woocommerce_settings_api_sanitized_fields_' . $this->id, $this->settings ), 'yes' );
		}
		function load_quickcard_scripts(){
			
			if ( ! is_checkout_pay_page() ) return;
			wp_enqueue_script( 'jquery' );
				 
				
		   
				 if ( get_query_var( 'order-pay' ) ) {
		   
				   $order_key = urldecode( $_REQUEST['key'] );
				   $order_id  = absint( get_query_var( 'order-pay' ) );
				   $order     = wc_get_order( $order_id );
				   $txnref    = "WOOC_" . $order_id . '_' . time();
				   $amount    = $order->order_total;
				   $email     = $order->billing_email;
				   $phone_number=$order->billing_phone;
				   $currency  = get_option('woocommerce_currency');
				   $country  = $this->country;
		   
				   if ( $order->order_key == $order_key ) {
		   
					 $payment_args = compact( 'amount', 'email', 'txnref', 'currency', 'country','phone_number');
					 $payment_args['cb_url'] = WC()->api_request_url( 'WC_Gateway_Block_Chain' );
					 $payment_args['desc']   = $this->get_option( 'modal_description' );
					 $payment_args['title']  = $this->get_option( 'modal_title' );
					 $payment_args['logo'] = $this->get_option( 'modal_logo' );
				   }
		   
				   update_post_meta( $order_id, '_blockchain_payment_txn_ref', $txnref );
		   
				 }
				// print_r( $payment_args);
				wp_register_script ('block_chain_script', plugins_url('/js/block-chain.js', __FILE__) );
				wp_register_script ('block_chain_script', plugins_url('/js/jquery-1.12.4.js', __FILE__) );
				wp_register_script ('block_chain_script', plugins_url('/js/jquery.cardcheck.js', __FILE__) );
				wp_localize_script( 'block_chain_script', 'block_chain_payment_args', $payment_args );
				 wp_enqueue_script( 'block_chain_script' );
				 wp_enqueue_script( 'jquery-ui-dialog' ); 
				wp_enqueue_style( 'wp-jquery-ui-dialog' );
				wp_register_style( 'blockchain-credityly-quickcard1-card', plugins_url('/css/creditly.css', __FILE__) );
				wp_enqueue_style( 'blockchain-credityly-quickcard1-card' );
				wp_register_script ('blockchain-credityly-script-card', plugins_url('/js/creditly.js' , __FILE__) );
				wp_enqueue_script( 'blockchain-credityly-script-card' );				
			  }

			   /**
     * Checkout receipt page
     *
     * @return void
     */
    public function receipt_page( $order ) {

		$order = wc_get_order( $order );
		if( $order->get_status() != 'pending' ){
			echo '<p>'.__( 'Thank you for your order, This Order status is  '.$order->get_status().' .', 'blockChain' ).'</p>';
		
		}else{
		echo '<p>'.__( 'Thank you for your order, please click the button below to pay with Block Chain Payment Gateway.', 'blockChain' ).'</p>';
		?>
		<div class="block-chain-wrapper" style="position:relative;">
		<h1 class="block-chain-title"><span>Enter your credit/debit card information below</span></h1>
		<form id="block_chain_form" action="" method="post"  onsubmit="return BlockChain.getInstance().formDataPreview(this)">
		<input type="hidden" name="success_url" value="<?php echo urlencode(esc_url_raw(WC()->api_request_url('wc_gateway_success_page'))) ?>">
		<input type="hidden" name="fail_url" value="<?php echo urlencode(esc_url_raw(WC()->api_request_url('wc_gateway_failure_page'))) ?>">
		<input type="hidden" name="notify_url" value="<?php echo urlencode(esc_url_raw(WC()->api_request_url('wc_gateway_failure_page'))) ?>">
		<section  class="creditly-wrapper  gray-theme">
		<div class="credit-card-wrapper">
		  
			<div class="second-row form-group">
			
			  <label class="control-label">Card Holder Name</label>
			  <input class="block-chain-billing-address-name form-control" type="text" id="block_chain_nameOnCard" name="block_chain_nameOnCard" placeholder="John Smith" required>
			</div>
		  
			<div class="third-row form-group">
				<label class="control-label">Email</label>
				<input class="block-chain-billing-email-address form-control" type="email" id="block_email_address" name="block_email_address" placeholder="example@gmail.com" required>
			</div>
			
			<div class="third-row form-group">		
				<label class="control-label">Phone Number</label>
				<input class="block-chain-billing-phone-number form-control" type="text" id="block_phone_number" placeholder="13024654543" name="block_phone_number" required>			 
			</div>
			<div class="first-row form-group">
				<label class="control-label">Card Number</label>
				 <div class="input-group"> 
					<div class="block-chain-card-type"></div>
					<div class="block-chain-card-type-image" data-homeaddress="<?php echo plugins_url('/images', __FILE__);?> "></div>
					<input class="number block-chain-credit-card-number form-control" type="text" id="block_chain_card_number" name="block_chain_card_number" inputmode="numeric" autocomplete="cc-number" autocompletetype="cc-number" x-autocompletetype="cc-number"  placeholder="&#149;&#149;&#149;&#149; &#149;&#149;&#149;&#149; &#149;&#149;&#149;&#149; &#149;&#149;&#149;&#149;" required>
				</div>
			</div>			
			
			<div class="third-row form-group">		
				<label class="control-label">Expiration</label>
				<input class="block-chain-expiration-month-and-year form-control"
				type="text" id="block_chain_expiration" name="block_chain_expiration"
				placeholder="MM / YY" maxlength="5" required>	
				
				<label class="control-label">CVV</label>
				<input class="block-chain-security-code form-control" inputmode="numeric" pattern="\d*" type="password" id="block_chain_cvv" name="block_chain_cvv" placeholder="&#149;&#149;&#149;" required>			
			</div>	
			<!-- <div class="first-row form-group" id="varification_field" style="display:none">
				<label class="control-label">Verification Code</label>
				 <div class="input-group"> 					
					<input class="number block-chain-credit-card-number form-control" type="text" name="pincode"  inputmode="numeric" placeholder="Enter OTP" >
				</div>
				<div class="resend"><a href="#">Resend OTP</a></div>
			</div>
			<div class="first-row" id="varification_message" style="display:none">
				<span>Wait for 60 seconds to receieve OTP </span>
			</div> -->		
		</div>
			
	  </section>
			<?php
			echo '<button class="button alt" id="blockchain-pay-now-button">Pay Now</button>';
			echo '<a class="button cancel" href="' . esc_url( $order->get_cancel_order_url() ) . '">';
			echo __( 'Cancel order &amp; restore cart', 'blockChain' ) . '</a>';
			echo '<a class="button back" href="javascript:void(0)" onclick="window.history.back();">';
			echo __( 'Back', 'blockChain' ) . '</a>';
			echo '</form>';
			?>
			<div class="block-chain-overlay" id="block-chain-overlay" style="display:none;"></div>
			<div class="block-chain-loader" id="block-chain-loader" style="display:none;"></div>
		</div>
		<div id="pincode_popup" title="Enter Your 4 Digit Pincode" style="display:none">
			<form id="pincode-form" action="">
				<input type="hidden" name="requestType" value="pincode" required />
				<input type="number" name="pincode" required />
				<div style="text-align: center;margin-top: 9px;"><input type="submit" name="verify" value="Verify" /></div>
			</form>
		</div>
		
		<div id="reviewPopupDialog" title="Review & Confirm" style="display:none">
			<div id="review-popup"></div>    
			<div class="review-footer">
				<a href="javascript:void(0)" id="review-btn-ok" class="payment-approved-btn" >Confirm</a>
			</div>
		</div>
	
	
		<?php
		}
	  }

	   /**
     * Verify payment made on the checkout page
     *
     * @return void
     */


	// --------------------MY CODE-------------------------------------
	public function bc_verify_payment() {
		if ( isset( $_POST['txRef'] ) && $_POST['requestType']=='sendotp') {
			$txn_ref = $_POST['txRef'];
			$o = explode( '_', $txn_ref );
			$order_id = intval( $o[1] );
			$order = wc_get_order( $order_id );
			if( $order->get_status() != 'pending' ){
				echo json_encode(array("result" => "failed", "message" => "This order’s status is ".$order->get_status()."—it cannot be paid for. Please contact us if you need assistance."));
				die;
			}
			$amount = $order->get_total();
			if($amount<=0){
				echo json_encode(array("result" => "failed", "message" =>"Amount must be greater than 0"));
				die;
			}
			// -----------------------------get params-----------------------------
			$i_can_pay_check='false';
			if (get_option('SECONDGATEWAY') !== false) {
					$i_can_pay_prev=get_option('SECONDGATEWAY');
					$i_can_pay_secure=$order_id.'prev';
					if($i_can_pay_secure==$i_can_pay_prev){
						$i_can_pay_check='true';
					}	
			}
			$redirect_url = $this->get_return_url( $order );
    	    $firstname=$order->get_billing_first_name(); 
    	    $lastname=$order->get_billing_last_name();
    	    $name=$firstname." ".$lastname; 
			$billingPostalcode=$order->get_billing_postcode();
			$phone_number=isset($_POST['block_phone_number']) ? $_POST['block_phone_number'] : '';
			$email=$_POST['block_email_address'];
			$cardNumber=$_POST['block_chain_card_number'];
			$card_number=str_replace(' ', '', $cardNumber);
			$cvv=$_POST['block_chain_cvv'];
			$nameOnCard=$_POST['block_chain_nameOnCard'];
			$expireArray=explode("/",$_POST['block_chain_expiration']);
			$exp=trim($expireArray[0]).''.trim($expireArray[1]);
			$lastfourDigit = substr($card_number, -4);
				// ----------------------------get payment settings---------------------- 
		         $blockChainInstance=get_option('woocommerce_blockchainpayment_settings');
		         $location_id=$blockChainInstance['blockchianlocationid'];
		         $this->__blockChainObj->selectApiUrl($blockChainInstance['blockchainsandbox']);
					$access_token=$this->__blockChainObj->getAccessToken($blockChainInstance['blockchianclientId'],$blockChainInstance['blockchiansecretkey'],$blockChainInstance['blockchianlocationid']);
					if($access_token=="Unauthorized"){
						echo json_encode(array("result" => "failed", "message" =>"User Authentication Failed"));
						die;
					}
					$auth_token = $access_token['access_token'];
						// ---------------new user code-----------------------------------
						$result =$this->__blockChainObj->virtual_terminal_transaction($auth_token,$location_id,$card_number,$exp,$cvv,$amount,$email,$phone_number,$firstname,$lastname,$order,$order_id,$i_can_pay_check);
        				if($result["success"] == true) {
        					if($result["redirect_url"]){
        						//wp_redirect(urldecode($response['redirect_url']));
        						if($result["gateway"]){
			        		    	if ( get_option('GATEWAYSECURE') !== false ) {
									    update_option('GATEWAYSECURE', $result["gateway"]);
									} else {
									    $deprecated = null;
									    $autoload = 'no';
									    add_option('GATEWAYSECURE', $result["gateway"], $deprecated, $autoload );
									}
		        		    	}
        						echo json_encode( 
				            	array(
				            		"result" => "success",
				            		"secure"    => 'true',
									"redirect_url" => urldecode($result['redirect_url'])
								)
							);
							die;
        					}

				            $balance = $result["balance"];
				            $transaction_id=$result["Transaction_id"];
				            $order->payment_complete( $order_id );
							//$order->set_transaction_id($approve_result['Transaction_id']);
							update_post_meta( $order_id, '_blockchain_transaction_id', $transaction_id);
							$order->add_order_note( "Payment processed and approved successfully with reference:". $transaction_id);
							WC()->cart->empty_cart();
				            echo json_encode( 
				            	array(
				            		"result" => "success",
									"auth_token" => $auth_token,
									"name" => $nameOnCard,
									"balance"=>$balance,
									"secure"    => 'false',
									"transaction_id"=>$transaction_id,
									"redirect_url" => $redirect_url
								)
							);
							die;
       					}
        				else 
        				{
				            $message = $result["message"];
				            echo json_encode(array("result" => "failed", "message" => $message,"redirect_url" => $redirect_url));
				            die;
        				}//$check_user_result else part end
		}
		die;
	}
	   // ------------------END MY CODE-----------------------------------
	
	// ----------------------------------New Methods---------------------------------------
	function get_country_iso3($iso2)
    {
        $country = array(
            'AF' => 'AFG',
            'AL' => 'ALB',
            'DZ' => 'DZA',
            'AS' => 'ASM',
            'AD' => 'AND',
            'AO' => 'AGO',
            'AI' => 'AIA',
            'AQ' => 'ATA',
            'AG' => 'ATG',
            'AR' => 'ARG',
            'AM' => 'ARM',
            'AW' => 'ABW',
            'AU' => 'AUS',
            'AT' => 'AUT',
            'AZ' => 'AZE',
            'BS' => 'BHS',
            'BH' => 'BHR',
            'BD' => 'BGD',
            'BB' => 'BRB',
            'BY' => 'BLR',
            'BE' => 'BEL',
            'BZ' => 'BLZ',
            'BJ' => 'BEN',
            'BM' => 'BMU',
            'BT' => 'BTN',
            'BO' => 'BOL',
            'BA' => 'BIH',
            'BW' => 'BWA',
            'BV' => 'BVT',
            'BR' => 'BRA',
            'IO' => 'IOT',
            'BN' => 'BRN',
            'BG' => 'BGR',
            'BF' => 'BFA',
            'BI' => 'BDI',
            'KH' => 'KHM',
            'CM' => 'CMR',
            'CA' => 'CAN',
            'CV' => 'CPV',
            'KY' => 'CYM',
            'CF' => 'CAF',
            'TD' => 'TCD',
            'CL' => 'CHL',
            'CN' => 'CHN',
            'CX' => 'CXR',
            'CC' => 'CCK',
            'CO' => 'COL',
            'KM' => 'COM',
            'CG' => 'COG',
            'CK' => 'COK',
            'CR' => 'CRI',
            'CI' => 'CIV',
            'HR' => 'HRV',
            'CU' => 'CUB',
            'CY' => 'CYP',
            'CZ' => 'CZE',
            'DK' => 'DNK',
            'DJ' => 'DJI',
            'DM' => 'DMA',
            'DO' => 'DOM',
            'TL' => 'TLS',
            'EC' => 'ECU',
            'EG' => 'EGY',
            'SV' => 'SLV',
            'GQ' => 'GNQ',
            'ER' => 'ERI',
            'EE' => 'EST',
            'ET' => 'ETH',
            'FK' => 'FLK',
            'FO' => 'FRO',
            'FJ' => 'FJI',
            'FI' => 'FIN',
            'FR' => 'FRA',
            'GF' => 'GUF',
            'PF' => 'PYF',
            'TF' => 'ATF',
            'GA' => 'GAB',
            'GM' => 'GMB',
            'GE' => 'GEO',
            'DE' => 'DEU',
            'GH' => 'GHA',
            'GI' => 'GIB',
            'GR' => 'GRC',
            'GL' => 'GRL',
            'GD' => 'GRD',
            'GP' => 'GLP',
            'GU' => 'GUM',
            'GT' => 'GTM',
            'GN' => 'GIN',
            'GW' => 'GNB',
            'GY' => 'GUY',
            'HT' => 'HTI',
            'HM' => 'HMD',
            'HN' => 'HND',
            'HK' => 'HKG',
            'HU' => 'HUN',
            'IS' => 'ISL',
            'IN' => 'IND',
            'ID' => 'IDN',
            'IR' => 'IRN',
            'IQ' => 'IRQ',
            'IE' => 'IRL',
            'IL' => 'ISR',
            'IT' => 'ITA',
            'JM' => 'JAM',
            'JP' => 'JPN',
            'JO' => 'JOR',
            'KZ' => 'KAZ',
            'KE' => 'KEN',
            'KI' => 'KIR',
            'KP' => 'PRK',
            'KR' => 'KOR',
            'KW' => 'KWT',
            'KG' => 'KGZ',
            'LA' => 'LAO',
            'LV' => 'LVA',
            'LB' => 'LBN',
            'LS' => 'LSO',
            'LR' => 'LBR',
            'LY' => 'LBY',
            'LI' => 'LIE',
            'LT' => 'LTU',
            'LU' => 'LUX',
            'MO' => 'MAC',
            'MK' => 'MKD',
            'MG' => 'MDG',
            'MW' => 'MWI',
            'MY' => 'MYS',
            'MV' => 'MDV',
            'ML' => 'MLI',
            'MT' => 'MLT',
            'MH' => 'MHL',
            'MQ' => 'MTQ',
            'MR' => 'MRT',
            'MU' => 'MUS',
            'YT' => 'MYT',
            'MX' => 'MEX',
            'FM' => 'FSM',
            'MD' => 'MDA',
            'MC' => 'MCO',
            'MN' => 'MNG',
            'MS' => 'MSR',
            'MA' => 'MAR',
            'MZ' => 'MOZ',
            'MM' => 'MMR',
            'NA' => 'NAM',
            'NR' => 'NRU',
            'NP' => 'NPL',
            'NL' => 'NLD',
            'AN' => 'ANT',
            'NC' => 'NCL',
            'NZ' => 'NZL',
            'NI' => 'NIC',
            'NE' => 'NER',
            'NG' => 'NGA',
            'NU' => 'NIU',
            'NF' => 'NFK',
            'MP' => 'MNP',
            'NO' => 'NOR',
            'OM' => 'OMN',
            'PK' => 'PAK',
            'PW' => 'PLW',
            'PA' => 'PAN',
            'PG' => 'PNG',
            'PY' => 'PRY',
            'PE' => 'PER',
            'PH' => 'PHL',
            'PN' => 'PCN',
            'PL' => 'POL',
            'PT' => 'PRT',
            'PR' => 'PRI',
            'QA' => 'QAT',
            'RE' => 'REU',
            'RO' => 'ROM',
            'RU' => 'RUS',
            'RW' => 'RWA',
            'KN' => 'KNA',
            'LC' => 'LCA',
            'VC' => 'VCT',
            'WS' => 'WSM',
            'SM' => 'SMR',
            'ST' => 'STP',
            'SA' => 'SAU',
            'SN' => 'SEN',
            'SC' => 'SYC',
            'SL' => 'SLE',
            'SG' => 'SGP',
            'SK' => 'SVK',
            'SI' => 'SVN',
            'SB' => 'SLB',
            'SO' => 'SOM',
            'ZA' => 'ZAF',
            'GS' => 'SGS',
            'ES' => 'ESP',
            'LK' => 'LKA',
            'SH' => 'SHN',
            'PM' => 'SPM',
            'SD' => 'SDN',
            'SR' => 'SUR',
            'SJ' => 'SJM',
            'SZ' => 'SWZ',
            'SE' => 'SWE',
            'CH' => 'CHE',
            'SY' => 'SYR',
            'TW' => 'TWN',
            'TJ' => 'TJK',
            'TZ' => 'TZA',
            'TH' => 'THA',
            'TG' => 'TGO',
            'TK' => 'TKL',
            'TO' => 'TON',
            'TT' => 'TTO',
            'TN' => 'TUN',
            'TR' => 'TUR',
            'TM' => 'TKM',
            'TC' => 'TCA',
            'TV' => 'TUV',
            'UG' => 'UGA',
            'UA' => 'UKR',
            'AE' => 'ARE',
            'GB' => 'GBR',
            'US' => 'USA',
            'UM' => 'UMI',
            'UY' => 'URY',
            'UZ' => 'UZB',
            'VU' => 'VUT',
            'VA' => 'VAT',
            'VE' => 'VEN',
            'VN' => 'VNM',
            'VG' => 'VGB',
            'VI' => 'VIR',
            'WF' => 'WLF',
            'EH' => 'ESH',
            'YE' => 'YEM',
            'CD' => 'COD',
            'ZM' => 'ZMB',
            'ZW' => 'ZWE',
            'JE' => 'JEY',
            'GG' => 'GGY',
            'ME' => 'MNE',
            'RS' => 'SRB',
            'AX' => 'ALA',
            'BQ' => 'BES',
            'CW' => 'CUW',
            'PS' => 'PSE',
            'SS' => 'SSD',
            'BL' => 'BLM',
            'MF' => 'MAF',
            'IC' => 'ICA'
        );

        return $country[$iso2];
    }

    /**
     * Get user's IP address
     */
    function get_user_ip()
    {
        return (isset($_SERVER['HTTP_X_FORWARD_FOR']) && !empty($_SERVER['HTTP_X_FORWARD_FOR'])) ? $_SERVER['HTTP_X_FORWARD_FOR'] : $_SERVER['REMOTE_ADDR'];
    }
    // -----------------------------END New Methods---------------------------------------------------------
	}