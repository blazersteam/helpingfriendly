<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_blog_no_sidebar()
{
    return array(
        array(
            'key' => 'layout_blog',
            'value' => 'col-1c'
        ),
        array(
            'key' => 'blog_design',
            'value' => 'grid_3'
        ),
        array(
            'key' => 'blog_excerpt_length',
            'value' => 40
        ),
        array(
            'key' => 'blog_thumbnail_height_mode',
            'value' => 'custom'
        ),
        array(
            'key' => 'blog_thumbnail_height_custom',
            'value' => '45%'
        ),
        array(
            'key' => 'blog_post_column',
            'value' => array(
                'xlg' => 1,
                'lg' => 1,
                'md' => 1,
                'sm' => 1,
                'xs' => 1,
                'mb' => 1
            )
        ),
        array(
            'filter_name' => 'veera/filter/page_title',
            'value' => '<header><h1 class="page-title">Blog No Sidebar</h1></header>'
        ),
        array(
            'filter_name' => 'veera/setting/option/get_single',
            'filter_func' => function( $value, $key ){
                if( $key == 'la_custom_css'){
                    $value .= '
.blog-main-loop.grid-3 .loop__item__info {
    text-align: left;
    padding-left: 8%;
    padding-right: 8%;
}
.blog-main-loop.grid-3 .loop__item__info2{
  max-width: 770px;
  margin: 0 auto;
}
';
                }
                return $value;
            },
            'filter_priority'  => 10,
            'filter_args'  => 2
        ),
    );
}