<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_blog_right_sidebar()
{
    return array(

        array(
            'key' => 'blog_design',
            'value' => 'list_2'
        ),
        array(
            'key' => 'blog_excerpt_length',
            'value' => 20
        ),
        array(
            'key' => 'blog_thumbnail_height_mode',
            'value' => 'custom'
        ),
        array(
            'key' => 'blog_thumbnail_height_custom',
            'value' => '50%'
        ),
        array(
            'filter_name' => 'veera/filter/page_title',
            'value' => '<header><h1 class="page-title">Blog Right Sidebar</h1></header>'
        ),
    );
}