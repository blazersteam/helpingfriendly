<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_home_15()
{
    return array(
        array(
            'key' => 'header_layout',
            'value' => '8'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'no'
        ),
        array(
            'key' => 'enable_header_top',
            'value' => 'yes'
        ),
        array(
            'key' => 'enable_searchbox_header',
            'value' => 'yes'
        ),
        array(
            'key' => 'header_top_elements',
            'value' => array(
                array(
                    'type' => 'link_text',
                    'icon' => 'fa fa-envelope-o',
                    'text' => 'info@la-studioweb.com',
                    'link' => 'mailto:info@la-studioweb.com',
                    'el_class' => 'hidden-xs'
                ),
                array (
                    'type' => 'text',
                    'icon' => 'fa fa-clock-o',
                    'text' => '9:00 AM - 21:00 PM',
                    'el_class' => 'hidden-xs'
                ),

                array (
                    'type' => 'dropdown_menu',
                    'text' => 'Currency',
                    'icon' => 'fa fa-dollar',
                    'menu_id' => 74,
                    'el_class' => 'la_com_dropdown_show_arrow la_com_dropdown_currency'
                ),
                array (
                    'type' => 'dropdown_menu',
                    'text' => 'Language',
                    'menu_id' => 73,
                    'icon' => 'fa fa-globe',
                    'el_class' => 'la_com_dropdown_show_arrow la_com_dropdown_language'
                ),
            )
        ),

        array(
            'key' => 'header_access_icon_1',
            'value' => array(
                array(
                    'type' => 'dropdown_menu',
                    'icon' => 'fa fa-user',
                    'menu_id' => 72,
                    'el_class' => ''
                ),
                array(
                    'type' => 'cart',
                    'icon' => 'dl-icon-cart1',
                    'link' => '#',
                    'el_class' => ''
                )
            )
        ),

        array(
            'key' => 'header_height',
            'value' => '120px'
        ),

        array(
            'key' => 'megamenu_lv2|megamenu_lv3',
            'value' => '13px'
        ),

        array(
            'key' => 'mm_lv_1_color|mm_lv_1_hover_color',
            'value' => '#fff'
        ),

        array(
            'key' => 'footer_layout',
            'value' => '3col444'
        ),


        array(
            'key' => 'footer_space',
            'value' => array(
                'padding_top'       => '40px',
                'padding_bottom'    => '0'
            )
        ),

        array(
            'filter_name' => 'veera/filter/footer_column_1',
            'value' => 'footer-layout-3-column-1'
        ),
        array(
            'filter_name' => 'veera/filter/footer_column_2',
            'value' => 'footer-layout-3-column-2'
        ),
        array(
            'filter_name' => 'veera/filter/footer_column_3',
            'value' => 'footer-layout-3-column-3'
        ),

        array(
            'filter_name' => 'veera/setting/option/get_single',
            'filter_func' => function( $value, $key ){
                if( $key == 'la_custom_css'){
                    $value .= '
.header-v8 .site-header__nav-primary {
    background-color: #2C2929;
}
.site-category-nav .mega-menu .mm-popup-wide .popup > .inner {
    background-image: none !important;
}
.site-category-nav .mega-menu .mm-popup-wide .popup > .inner > ul.sub-menu > li {
    min-width: 22%;
}
.site-main-nav .main-menu > li > a{
    font-weight: 400;
}

.site-footer .widget .widget-title{
    font-size: 14px;
}
@media(min-width: 992px){
    .la-footer-3col444 .footer-column-inner {
        max-width: 100%;
        margin: 0 auto;
        width: 270px;
    }
    .la-footer-3col444 .footer-column-1 .footer-column-inner{
        float: left;
    }
    .la-footer-3col444 .footer-column-3 .footer-column-inner{
        float: right;
    }
}
';
                }
                return $value;
            },
            'filter_priority'  => 10,
            'filter_args'  => 2
        ),
        array(
            'key' => 'footer_copyright',
            'value' => '
<div class="row font-size-11">
	<div class="col-xs-12 col-sm-4 xs-text-left text-color-secondary">
		[wp_nav_menu menu_id="34"]
	</div>
	<div class="col-xs-12 col-sm-4 text-center xs-text-left xs-pt-5 xs-pb-10">
		© 2018 Veera All rights reserved
	</div>
	<div class="col-xs-12 col-sm-4 text-right xs-text-left">
		<img src="//veera.la-studioweb.com/wp-content/themes/veera/assets/images/payments.png" alt="payment">
	</div>
</div>
'
        ),
    );
}