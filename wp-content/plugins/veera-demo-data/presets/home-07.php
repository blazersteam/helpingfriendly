<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_home_07()
{
    return array(

        array(
            'key' => 'header_transparency',
            'value' => 'yes'
        ),

        array(
            'key' => 'header_layout',
            'value' => '5'
        ),

        array(
            'key' => 'header_height',
            'value' => '120px'
        ),


        array(
            'key' => 'footer_layout',
            'value' => '3col363'
        ),

        array(
            'key' => 'footer_link_color',
            'value' => '#262626'
        ),

        array(
            'key' => 'footer_space',
            'value' => array(
                'padding_top'       => '45px',
                'padding_bottom'    => '0'
            )
        ),

        array(
            'filter_name' => 'veera/filter/footer_column_1',
            'value' => 'footer-layout-4-column-1'
        ),
        array(
            'filter_name' => 'veera/filter/footer_column_2',
            'value' => 'footer-layout-4-column-2'
        ),
        array(
            'filter_name' => 'veera/filter/footer_column_3',
            'value' => 'footer-layout-4-column-3'
        ),

        array(
            'filter_name' => 'veera/setting/option/get_single',
            'filter_func' => function( $value, $key ){
                if( $key == 'la_custom_css'){
                    $value .= '
.footer-bottom .footer-bottom-inner{
    border: none;
}
';
                }
                return $value;
            },
            'filter_priority'  => 10,
            'filter_args'  => 2
        ),
        array(
            'key' => 'footer_copyright',
            'value' => '
<div class="row font-size-11">
	<div class="col-xs-12 text-center">
		© 2018 Veera All rights reserved
	</div>
</div>
'
        ),
    );
}