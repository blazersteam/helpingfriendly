<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_blog_03_columns()
{
    return array(
        array(
            'key' => 'layout_blog',
            'value' => 'col-1c'
        ),
        array(
            'key' => 'blog_design',
            'value' => 'grid_3'
        ),
        array(
            'key' => 'blog_excerpt_length',
            'value' => 20
        ),
        array(
            'key' => 'blog_thumbnail_height_mode',
            'value' => '4-3'
        ),
        array(
            'key' => 'blog_post_column',
            'value' => array(
                'xlg' => 3,
                'lg' => 3,
                'md' => 3,
                'sm' => 2,
                'xs' => 1,
                'mb' => 1
            )
        ),
        array(
            'filter_name' => 'veera/filter/page_title',
            'value' => '<header><h1 class="page-title">Blog 03 Columns</h1></header>'
        ),
    );
}