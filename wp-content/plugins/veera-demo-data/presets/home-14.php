<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_home_14()
{
    return array(
        array(
            'key' => 'header_layout',
            'value' => 3
        ),

        array(
            'key' => 'header_access_icon_1',
            'value' => array(
                array(
                    'type' => 'search_1',
                    'el_class' => ''
                ),
                array(
                    'type' => 'dropdown_menu',
                    'icon' => 'fa fa-user',
                    'menu_id' => 72,
                    'el_class' => ''
                ),
                array(
                    'type' => 'cart',
                    'icon' => 'dl-icon-cart1',
                    'link' => '#',
                    'el_class' => ''
                )
            )
        ),

        array(
            'key' => 'footer_layout',
            'value' => '1col'
        ),

        array(
            'key' => 'footer_background',
            'value' => array(
                'color'             => '#262626',
            )
        ),
        array(
            'key' => 'footer_space',
            'value' => array(
                'padding_top'       => '50px',
                'padding_bottom'    => '0'
            )
        ),

        array(
            'key' => 'footer_text_color|footer_link_hover_color|footer_copyright_text_color|footer_copyright_link_hover_color',
            'value' => '#8D8D8D'
        ),

        array(
            'key' => 'footer_heading_color|footer_link_color|footer_copyright_link_color',
            'value' => '#fff'
        ),
        array(
            'key' => 'footer_copyright_background_color',
            'value' => '#262626'
        ),

        array(
            'filter_name' => 'veera/filter/footer_column_1',
            'value' => 'footer-layout-2'
        ),

        array(
            'filter_name' => 'veera/setting/option/get_single',
            'filter_func' => function( $value, $key ){
                if( $key == 'la_custom_css'){
                    $value .= '

.footer-bottom .footer-bottom-inner{
    border: none;
}
';
                }
                return $value;
            },
            'filter_priority'  => 10,
            'filter_args'  => 2
        ),
        array(
            'key' => 'footer_copyright',
            'value' => '
<div class="row font-size-11">
	<div class="col-xs-12 col-sm-6 xs-text-center">
		© 2018 Veera All rights reserved
	</div>
	<div class="col-xs-12 col-sm-6 text-right xs-text-center font-size-14 text-color-white">
		[la_social_link]
	</div>
</div>
'
        ),
    );
}