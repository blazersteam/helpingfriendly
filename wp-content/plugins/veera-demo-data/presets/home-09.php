<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_veera_preset_home_09()
{
    return array(

        array(
            'key' => 'enable_header_top',
            'value' => 'custom'
        ),
        array(
            'key' => 'use_custom_header_top',
            'value' => '[vc_message]Huge sale discount weekend up 70%! <a href="/shop/">Shop now</a>[/vc_message]'
        ),
        array(
            'key' => 'header_layout',
            'value' => '11'
        ),

        array(
            'key' => 'header_height',
            'value' => '100px'
        ),

        array(
            'key' => 'header_full_width',
            'value' => 'yes'
        ),

        array(
            'key' => 'header_top_background_color',
            'value' => '#D25B5B'
        ),
        array(
            'key' => 'header_top_text_color|header_top_link_color',
            'value' => '#8D8D8D'
        ),

        array(
            'key' => 'header_access_icon_2',
            'value' => array(
                array(
                    'type' => 'aside_header',
                    'icon' => 'dl-icon-menu1',
                    'el_class' => ''
                )
            )
        ),

        array(
            'key' => 'header_access_icon_1',
            'value' => array(
                array(
                    'type' => 'search_1',
                    'el_class' => ''
                ),
                array(
                    'type' => 'dropdown_menu',
                    'icon' => 'fa fa-user',
                    'menu_id' => 72,
                    'el_class' => ''
                ),
                array(
                    'type' => 'cart',
                    'icon' => 'dl-icon-cart1',
                    'link' => '#',
                    'el_class' => ''
                )
            )
        ),

        array(
            'key' => 'footer_layout',
            'value' => '3col444'
        ),

        array(
            'key' => 'footer_space',
            'value' => array(
                'padding_top'       => '40px',
                'padding_bottom'    => '0'
            )
        ),

        array(
            'filter_name' => 'veera/filter/footer_column_1',
            'value' => 'footer-layout-3-column-1'
        ),
        array(
            'filter_name' => 'veera/filter/footer_column_2',
            'value' => 'footer-layout-3-column-2'
        ),
        array(
            'filter_name' => 'veera/filter/footer_column_3',
            'value' => 'footer-layout-3-column-3'
        ),

        array(
            'filter_name' => 'veera/setting/option/get_single',
            'filter_func' => function( $value, $key ){
                if( $key == 'la_custom_css'){
                    $value .= '
.enable-header-fullwidth:not(.body-boxed) .site-header-top > .container{
    width: 1200px;
}
.site-header-top{
    font-size: 1em;
    padding: 0;
}
.site-header-top.use-custom-html .vc_message_box {
    background: none;
    border: none;
    color: #fff;
    text-transform: uppercase;
    text-align: center;
    margin: 0;
    letter-spacing: 3px;
    font-weight: 300;
    padding: 20px 0;
}
.site-header-top.use-custom-html .vc_message_box .vc_message_box-icon{
    display: none;
}
.site-header-top.use-custom-html .vc_message_box a {
    font-weight: bold;
}
.site-main-nav .main-menu > li > a{
    font-weight: 400;
}

.site-footer .widget .widget-title{
    font-size: 14px;
}
@media(min-width: 992px){
    .la-footer-3col444 .footer-column-inner {
        max-width: 100%;
        margin: 0 auto;
        width: 270px;
    }
    .la-footer-3col444 .footer-column-1 .footer-column-inner{
        float: left;
    }
    .la-footer-3col444 .footer-column-3 .footer-column-inner{
        float: right;
    }
}
@media(max-width: 1200px){
.site-header-top.use-custom-html .vc_message_box {
    padding: 10px 0;
    letter-spacing: 1px;
}
}
@media(max-width: 600px){
.site-header-top.use-custom-html .vc_message_box {
    font-size: 12px;
    letter-spacing: 1px;
}
.site-header-top.use-custom-html .vc_message_box .close-button {
    margin-top: 0;
}
}
';
                }
                return $value;
            },
            'filter_priority'  => 10,
            'filter_args'  => 2
        ),
        array(
            'key' => 'footer_copyright',
            'value' => '
<div class="row font-size-11">
	<div class="col-xs-12 col-sm-4 xs-text-left text-color-secondary">
		[wp_nav_menu menu_id="34"]
	</div>
	<div class="col-xs-12 col-sm-4 text-center xs-text-left xs-pt-5 xs-pb-10">
		© 2018 Veera All rights reserved
	</div>
	<div class="col-xs-12 col-sm-4 text-right xs-text-left">
		<img src="//veera.la-studioweb.com/wp-content/themes/veera/assets/images/payments.png" alt="payment">
	</div>
</div>
'
        ),
    );
}